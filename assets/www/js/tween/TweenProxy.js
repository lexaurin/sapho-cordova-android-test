/**
 * @class TweenProxy
 * @param {number} duration
 * @param {Function} ease
 * @param {number} [defaultProgress=undefined]
 * @extends {EventDispatcher}
 * @constructor
 */
SaphoLauncher.TweenProxy = function TweenProxy(duration,ease,defaultProgress) {
    SaphoLauncher.EventDispatcher.call(this);

    this.progress = defaultProgress || 0.0;

    this._interval = -1;
    this._running = false;
    this._reversed = false;

    this._start = -1.0;
    this._end = -1.0;
    this._reversedEnd = -1.0;
    this._now = -1.0;
    this._duration = duration * 1000 || 1000;
    this._ease = ease || SaphoLauncher.Easing.linear;
    this._timeStamp = window.performance && window.performance.now ? window.performance : Date;
    this._intervalReference = this._tweenInterval.bind(this);
};

SaphoLauncher.TweenProxy.prototype = Object.create(SaphoLauncher.EventDispatcher.prototype);

/**
 * Set easing function
 * @method setEase
 * @param {Function} value
 */
SaphoLauncher.TweenProxy.prototype.setEase = function setEase(value) {
    this._ease = value;
};

/**
 * Start
 * @method start
 * @param {boolean} [reverseIfRunning=false] - reverse the tween if the tween is currently running
 * @param {Function} [ease=null] - set new ease
 */
SaphoLauncher.TweenProxy.prototype.start = function start(reverseIfRunning,ease) {
    if (ease) this.setEase(ease);

    if (!this._running) {
        this._running = true;
        this._reversed = false;

        this._tween();
    } else {
        if (reverseIfRunning) {
            if (this._reversed) {
                this._reversed = false;
            } else {
                this._reversed = true;
                this._reversedEnd = this._start + (this._now - this._start) * 2;
            }
        }
    }
};

/**
 * Stop
 * @method stop
 */
SaphoLauncher.TweenProxy.prototype.stop = function stop() {
    this._running = false;

    clearInterval(this._interval);
    this._interval = -1;
};

/**
 * Restart
 * @method restart
 */
SaphoLauncher.TweenProxy.prototype.restart = function restart() {
    if (this._running) this.stop();

    this.start();
};

/**
 * Is tween running?
 * @method isRunning
 * @returns {boolean}
 */
SaphoLauncher.TweenProxy.prototype.isRunning = function isRunning() {
    return this._running;
};

/**
 * Tween
 * @method _tween
 * @private
 */
SaphoLauncher.TweenProxy.prototype._tween = function _tween() {
    if (this._interval > 0) {
        clearInterval(this._interval);
        this._interval = -1.0;
    }

    this.progress = 0.0;

    this._start = this._timeStamp.now();
    this._end = this._start + this._duration;
    this._interval = setInterval(this._intervalReference,1000/120);
};

/**
 * Tween interval function
 * @method _tweenInterval
 * @private
 */
SaphoLauncher.TweenProxy.prototype._tweenInterval = function _tweenInterval() {
    this._now = this._timeStamp.now();

    var end = this._reversed ? this._reversedEnd : this._end;
    var progress = (this._duration - (end - this._now)) / this._duration;
    if (progress < 0) progress = 0.0;
    else if (progress > 1) progress = 1.0;

    this.progress = this._ease(progress);
    if(this._now >= end) {
        this.progress = 1.0;
        this.stop();

        this.dispatchEvent(SaphoLauncher.EventType.COMPLETE);
    }
};

/**
 * Destroy
 */
SaphoLauncher.TweenProxy.prototype.destroy = function destroy() {
    SaphoLauncher.EventDispatcher.prototype.destroy.call(this);

    this.stop();

    this._intervalReference = null;
    this._timeStamp = null;
    this._ease = null;
};
