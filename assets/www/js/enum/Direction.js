/**
 * Direction
 * @enum {string}
 * @return {{LEFT:string,RIGHT:string}}
 */
SaphoLauncher.Direction = {
    LEFT:"LEFT",
    RIGHT:"RIGHT"
};
