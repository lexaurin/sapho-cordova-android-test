/**
 * Error Message
 * @enum {string}
 * @return {{
 *      GOOGLE_AUTH_FAILED:string,
 *      NO_APP:string,
 *      NO_GOOGLE_APP:string,
 *      NO_GOOGLE_APP_MESSAGE:string
 *      UNABLE_TO_LOAD_APP:string,
 *      INVALID_SAPHO_APP:string
 * }}
 */
SaphoLauncher.ErrorMessage = {
    GOOGLE_AUTH_FAILED:"Google authentication failed",
    NO_APP:"No app available",
    NO_GOOGLE_APP:"There is no server assigned for your Google account",
    NO_GOOGLE_APP_MESSAGE:'To use your Google account first create an app at "try.sapho.com" or contact "sales@sapho.com" to register your Google for Work domain',
    UNABLE_TO_LOAD_APP:"Unable to load Sapho App",
    INVALID_SAPHO_APP:"Not a valid Sapho App URL"
};
