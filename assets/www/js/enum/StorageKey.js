/**
 * Storage Keys enum
 * @enum {string}
 * @type {{RECENT_URLS: string, SAPHO_APP: string, LAST_CONNECTION_URL: string,LAST_CONNECTION:string}}
 */
SaphoLauncher.StorageKey = {
    RECENT_URLS:"recentUrls",
    SAPHO_APP:"saphoApp",
    LAST_CONNECTION:"lastConnection"
};
