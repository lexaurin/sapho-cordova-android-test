/**
 * Model Proxy state
 * @enum {string}
 * @return {{
 *      TICKER:string,
 *      STORAGE:string
 * }}
 */
SaphoLauncher.ModelName = {
    TICKER:"SaphoLauncher.ModelName.TICKER",
    STORAGE:"SaphoLauncher.ModelName.STORAGE"
};
