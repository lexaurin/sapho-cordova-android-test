/**
 * Screen Name
 * @enum {string}
 * @return {{BUTTON:string,CONNECTING:string,LOGIN:string}}
 */
SaphoLauncher.ScreenName = {
    BUTTON:"SaphoLauncher.ScreenName.BUTTON",
    CONNECTING:"SaphoLauncher.ScreenName.CONNECTING",
    LOGIN:"SaphoLauncher.ScreenName.LOGIN"
};
