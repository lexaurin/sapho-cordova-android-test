/**
 * View Segment state
 * @enum {number}
 * @return {{APPLICATION_VIEW:string}}
 */
SaphoLauncher.ViewName = {
    APPLICATION_VIEW:"SaphoLauncher.ViewName.APPLICATION_VIEW"
};
