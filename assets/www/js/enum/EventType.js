/**
 * Event type
 * @enum {string}
 * @return {{
 *      INITIALIZE:string,
 *      CHANGE_SCREEN:string,
 *      LOAD_APP:string,
 *      GOOGLE_LOGIN:string,
 *      LAST_CONNECTION_STRATEGY:string,
 *      COMPLETE:string,
 *      UPDATE:string,
 *      ERROR:string,
 *      CHANGE:string,
 *      LAYOUT_UPDATE:string,
 *      TICK:string,
 *      RESIZE:string,
 *      CLICK:string,
 *      FOCUS:string,
 *      BLUR:string,
 *      KEY_PRESS:string,
 *      PASTE:string,
 *      TEXT_INPUT:string,
 *      INPUT:string,
 *      BACK_BUTTON:string,
 *      LOAD_START:string,
 *      EXIT:string
 * }}
 */
SaphoLauncher.EventType = {
    // Commands
    INITIALIZE:"INITIALIZE",
    CHANGE_SCREEN:"CHANGE_SCREEN",
    LOAD_APP:"LOAD_APP",
    GOOGLE_LOGIN:"GOOGLE_LOGIN",
    LAST_CONNECTION_STRATEGY:"LAST_CONNECTION_STRATEGY",

    COMPLETE:"COMPLETE",
    UPDATE:"UPDATE",
    ERROR:"ERROR",
    CHANGE:"change",
    LAYOUT_UPDATE:"LAYOUT_UPDATE",
    TICK:"TICK",

    // DOM
    RESIZE:"resize",
    CLICK:"click",
    FOCUS:"focus",
    BLUR:"blur",
    KEY_PRESS:"keypress",
    PASTE:"paste",
    TEXT_INPUT:"textInput",
    INPUT:"input",
    BACK_BUTTON:"backbutton",
    LOAD_START:"loadstart",
    EXIT:"exit"
};
