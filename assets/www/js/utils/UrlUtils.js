SaphoLauncher.UrlUtils = {
    /**
     * Remove parameters from url passed in
     * @method  removeParams
     * @param   {string} url
     * @param   {Array.<string>} paramsToRemove
     * @returns {string}
     */
    removeParams:function removeParams(url,paramsToRemove) {
        var components = url.split("?"),
            params = components && components.length > 1 ? components[1].split("&") : null,
            result = components[0];

        if (params) {
            result += "?";
            for (var i=0,l=params.length,param=null;i<l;i++) {
                param = params[i].split("=");
                if (param && param.length && param[0].length && paramsToRemove.indexOf(param[0]) === -1) {
                    result += param[0]+"="+param[1]+"&";
                }
            }
            if (result.charAt(result.length-1) === "&") result = result.substring(0,result.length-1);
        } else {
            result = url;
        }

        return result;
    },

    /**
     * Append parrams to the url passed in
     * @method  appendParams
     * @param   {string} appUrl
     * @param   {Array} params
     * @returns {string}
     */
    appendParams:function appendParams(appUrl,params) {
        var url = appUrl;
        if (url.indexOf("?") === -1) url += "?";

        for (var i=0,l=params.length;i<l;i++) {
            if (!i && url[url.length-1] !== "?") url += "&";
            url += params[i++] + "=" + params[i];
        }
        return url;
    },

    /**
     * Find and return value of parameter by the parameter name passed in
     * @param {string} paramName
     * @param {string} [url=null]
     */
    getParam:function getParam(paramName,url) {
        var results = new RegExp(paramName+"(=([^&#]*)|&|#|$)").exec(url ? url : this.url);
        return results && results[2];
    },

    /**
     * Parse and return host name
     * @method  getHostName
     * @param   {string} [url=null]
     * @param   {boolean} [includePort=false]
     * @returns {string}
     */
    getHostName:function getHostName(url,includePort) {
        var linkElement = document.createElement("a"),
            hostName = null;

        linkElement.href = url.indexOf("http") === -1 ? "http://"+url : url;
        hostName = linkElement.host;

        if (!includePort && hostName.indexOf(":") > -1) hostName = hostName.substring(0,hostName.indexOf(":"));

        return hostName;
    },

    /**
     * Parse and return path of the url passed in
     * @param {string} url
     * @returns {string}
     */
    getPathName:function getPathName(url) {
        var linkElement = document.createElement("a");
        linkElement.href = url;
        return linkElement.pathname;
    }
};
