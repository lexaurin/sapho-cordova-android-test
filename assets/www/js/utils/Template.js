function Template() {
    this._args = Array.prototype.slice.call(arguments,0);
    this._string = this._args.shift();
}

Template.prototype.toString = function() {
    return this._args.reduce(function(str,value,index) {
        return str.replace(new RegExp('\\{'+index+'\\}','gi'),value.join ? value.join("") : value);
    },this._string);
};
