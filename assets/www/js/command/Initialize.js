/**
 * @class Initialize
 * @extends {Command}
 * @param {{dispatchEvent:Function,stopExecution:Function}} controller
 * @constructor
 */
SaphoLauncher.Initialize = function Initialize(controller) {
    SaphoLauncher.Command.call(this,controller);
};

SaphoLauncher.Initialize.prototype = Object.assign(Object.create(SaphoLauncher.Command.prototype), {
    /**
     * Execute the command
     *
     * @method execute
     */
    execute:function execute() {
        var storage = new SaphoLauncher.Storage(window.localStorage);

        this._initModel(storage);
        this._initController();
        this._initView(SaphoLauncher.ScreenName);

        this._controller.dispatchEvent(SaphoLauncher.EventType.LAST_CONNECTION_STRATEGY,storage);

        this.dispatchEvent(SaphoLauncher.EventType.COMPLETE, this);
    },

    /**
     * Initialize application model
     *
     * @method _initModel
     * @param {SaphoLauncher.Storage} storage
     * @private
     */
    _initModel:function _initModel(storage) {
        SaphoLauncher.ModelLocator.init([
            SaphoLauncher.ModelName.TICKER, new SaphoLauncher.Ticker(),
            SaphoLauncher.ModelName.STORAGE, storage
        ]);
    },

    /**
     * Initialize commands
     *
     * @method _initController
     * @private
     */
    _initController:function _initController() {
        this._controller.init([
            SaphoLauncher.EventType.CHANGE_SCREEN, SaphoLauncher.ChangeScreen,
            SaphoLauncher.EventType.LOAD_APP, SaphoLauncher.LoadApp,
            SaphoLauncher.EventType.GOOGLE_LOGIN, SaphoLauncher.GoogleLogin,
            SaphoLauncher.EventType.LAST_CONNECTION_STRATEGY, SaphoLauncher.LastConnectionStrategy
        ]);
    },

    /**
     * Initialize view
     *
     * @method _initView
     * @param {Object} ScreenName
     * @private
     */
    _initView:function _initView(ScreenName) {
        window.StatusBar && window.StatusBar.backgroundColorByHexString("#000000");

        var layout = {width:window.innerWidth,height:window.innerHeight};
        var screens = Object.create(null);

        screens[ScreenName.BUTTON] = new SaphoLauncher.ButtonScreen($("#button-screen"),layout,this._controller);
        screens[ScreenName.CONNECTING] = new SaphoLauncher.ConnectingScreen($("#connecting-screen"),layout,this._controller);
        screens[ScreenName.LOGIN] = new SaphoLauncher.LoginScreen($("#login-screen"),layout,this._controller);

        SaphoLauncher.ViewLocator.addViewSegment(SaphoLauncher.ViewName.APPLICATION_VIEW, new SaphoLauncher.ApplicationView(layout,screens));
    }
});
