/**
 * @class ChangeScreen
 * @extends {Command}
 * @param {{dispatchEvent:Function,stopExecution:Function}} controller
 * @constructor
 */
SaphoLauncher.ChangeScreen = function ChangeScreen(controller) {
    SaphoLauncher.Command.call(this,controller);
};

SaphoLauncher.ChangeScreen.prototype = Object.create(SaphoLauncher.Command.prototype);

/**
 * Execute the command
 *
 * @method execute
 * @param {Object} data
 */
SaphoLauncher.ChangeScreen.prototype.execute = function execute(data) {
    SaphoLauncher.ViewLocator.getViewSegment(SaphoLauncher.ViewName.APPLICATION_VIEW).changeScreen(data);

    this.dispatchEvent(SaphoLauncher.EventType.COMPLETE,this);
};
