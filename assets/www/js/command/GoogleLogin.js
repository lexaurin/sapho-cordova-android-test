/**
 * @class GoogleLogin
 * @extends {Command}
 * @param {{dispatchEvent:Function,stopExecution:Function}} controller
 * @constructor
 */
SaphoLauncher.GoogleLogin = function GoogleLogin(controller) {
    SaphoLauncher.Command.call(this,controller);

    this._delay = 0;
    this._googleApiRequest = null;
    this._saphoAuthRequest = null;
    this._saphoAccountRequest = null;
    this._defaultServerUrl = "https://try.sapho.com";
    this._inAppBrowser = null;
    this._inAppBrowserListener = null;

    var storage = SaphoLauncher.ModelLocator.getProxy(SaphoLauncher.ModelName.STORAGE);
    this._googleAuth = new SaphoLauncher.GoogleAuth(storage);
    this._googleAccount = new SaphoLauncher.GoogleAccount(storage);
};

SaphoLauncher.GoogleLogin.prototype = Object.assign(Object.create(SaphoLauncher.Command.prototype),{
    /**
     * Execute the command
     * @param {{delay:number,switch:boolean}} data
     */
    execute:function execute(data) {
        this._delay = data && data.delay || 0;

        this._changeScreen(SaphoLauncher.ScreenName.CONNECTING,SaphoLauncher.ConnectingScreen.GOOGLE_TITLE," ",SaphoLauncher.Direction.LEFT);

        if (data && data.switch) this._switchAccounts();
        else this._authenticate();
    },

    /**
     * Open In-App-Browser with google-auth url and register IAB event listeners
     * @private
     */
    _authenticate:function _authenticate() {
        this._googleAccount.reset();
        this._inAppBrowser = cordova.InAppBrowser.open(encodeURI(this._googleAuth.authUrl),"_blank","location=no,hidden=yes");
        this._registerBrowserEventListeners();
    },

    /**
     * Log out from google account and authenticate again
     * @private
     */
    _switchAccounts:function _switchAccounts() {
        $.get(new Template("{0}/Logout?hl=en",this._googleAuth.host)).then(
            this._authenticate.bind(this),
            this._navigateToButtonScreen.bind(this)
        );
    },

    /**
     * InAppBrowser exit event listener
     * @param {Object} e
     * @private
     */
    _onBrowserExit:function _onBrowserExit(e) {
        this._unRegisterBrowserEventListeners();
        this._navigateToButtonScreen();
    },

    /**
     * InAppBrowser load start event handler
     * @param   {Object} e
     * @private
     */
    _onBrowserLoadStart:function _onBrowserLoadStart(e) {
        if (this._googleAuth.authenticated(e.url)) {
            this._unRegisterBrowserEventListeners();
            this._inAppBrowser.close();

            this._googleAccount.accessToken = SaphoLauncher.UrlUtils.getParam("access_token",e.url);

            this._googleApiRequest = $.get(new Template("https://www.googleapis.com/plus/v1/people/me?access_token={0}",this._googleAccount.accessToken)).then(
                this._onGoogleApiSuccess.bind(this),
                this._onRequestError.bind(this,SaphoLauncher.ErrorMessage.GOOGLE_AUTH_FAILED)
            );
        } else {
            if (this._googleAuth.formUrl(e.url)) {
                this._inAppBrowser.show();
            }
        }
    },

    /**
     * Google API request success handler
     * @param {{emails:Array.<{value:string}>}} response
     * @param {string} textStatus
     * @param {Object} request
     * @private
     */
    _onGoogleApiSuccess:function _onGoogleApiSuccess(response,textStatus,request) {
        this._googleAccount.email = response.emails[0].value;

        this._saphoAuthRequest = $.get(new Template("http://auth.sapho.com/api/v1/containers/domains/{0}",this._googleAccount.email)).then(
            this._onSaphoAuthSuccess.bind(this),
            this._onRequestError.bind(this,SaphoLauncher.ErrorMessage.NO_APP)
        );
    },

    /**
     * Sapho Auth server request success handler
     * @param {Object} response
     * @param {string} textStatus
     * @param {Object} request
     * @private
     */
    _onSaphoAuthSuccess:function _onSaphoAuthSuccess(response,textStatus,request) {
        if (response.host) {
            this._loadApp(response.host);
        } else {
            this._saphoAccountRequest = $.get(new Template("{0}/api/v1/auth/account/{1}/default",this._defaultServerUrl,this._googleAccount.email)).then(
                this._onSaphoAccountSuccess.bind(this),
                this._onRequestError.bind(this,SaphoLauncher.ErrorMessage.NO_GOOGLE_APP,SaphoLauncher.ErrorMessage.NO_GOOGLE_APP_MESSAGE)
            );
        }
    },

    /**
     * Sapho Account request success handler
     * @param {{lookupId:number,draftId:number,liveId:number,hasChanges:boolean,default:boolean}} response
     * @param {string} textStatus
     * @param {Object} request
     * @private
     */
    _onSaphoAccountSuccess:function _onSaphoAccountSuccess(response,textStatus,request) {
        this._loadApp(new Template("{0}/renderer?appid={1}",this._defaultServerUrl,response.lookupId).toString());
    },

    /**
     * API request error handler
     * @param {string} title
     * @param {string} [message=null]
     * @param {Object} [error=null]
     * @private
     */
    _onRequestError:function _onRequestError(title,message,error) {
        navigator.notification.confirm(message || '',this._onConfirmButtonPress.bind(this),title,['Switch Accounts','OK']);
    },

    /**
     * Confirm button press event handler
     * @param {number} buttonPressed
     * @private
     */
    _onConfirmButtonPress:function _onConfirmButtonPress(buttonPressed) {
        if (buttonPressed === 1) this._switchAccounts();
        else this._navigateToButtonScreen();
    },

    /**
     * Navigates to Button screen and completes this command
     * @private
     */
    _navigateToButtonScreen:function _navigateToButtonScreen() {
        this._changeScreen(SaphoLauncher.ScreenName.BUTTON);
        this.dispatchEvent(SaphoLauncher.EventType.COMPLETE,this);
    },

    /**
     * Load url passed in
     * @param {string} url
     * @private
     */
    _loadApp:function _loadApp(url) {
        url = SaphoLauncher.UrlUtils.appendParams(url || "",["gtoken",this._googleAccount.accessToken]);

        this._changeScreen(SaphoLauncher.ScreenName.CONNECTING,SaphoLauncher.ConnectingScreen.GOOGLE_TITLE,url);
        this._controller.dispatchEvent(SaphoLauncher.EventType.LOAD_APP,{url:url,delay:this._delay});

        this.dispatchEvent(SaphoLauncher.EventType.COMPLETE,this);
    },

    /**
     * Register In-App-Browser event listeners
     * @private
     */
    _registerBrowserEventListeners:function _registerBrowserEventListeners() {
        if (!this._inAppBrowserListener) {
            this._inAppBrowserListener = Object.create(null);
            this._inAppBrowserListener[SaphoLauncher.EventType.LOAD_START] = this._onBrowserLoadStart.bind(this);
            this._inAppBrowserListener[SaphoLauncher.EventType.EXIT] = this._onBrowserExit.bind(this);
        }
        this._inAppBrowser.addEventListener(SaphoLauncher.EventType.LOAD_START,this._inAppBrowserListener[SaphoLauncher.EventType.LOAD_START]);
        this._inAppBrowser.addEventListener(SaphoLauncher.EventType.EXIT,this._inAppBrowserListener[SaphoLauncher.EventType.EXIT]);
    },

    /**
     * UnRegister In-App-Browser event listeners
     * @private
     */
    _unRegisterBrowserEventListeners:function _unRegisterBrowserEventListeners() {
        this._inAppBrowser.removeEventListener(SaphoLauncher.EventType.LOAD_START,this._inAppBrowserListener[SaphoLauncher.EventType.LOAD_START]);
        this._inAppBrowser.removeEventListener(SaphoLauncher.EventType.EXIT,this._inAppBrowserListener[SaphoLauncher.EventType.EXIT]);
    },

    /**
     * Change screen based on arguments passed in
     * @param {string} name
     * @param {string} [title=null]
     * @param {string} [url=null]
     * @param {string} [direction=null]
     * @private
     */
    _changeScreen:function _changeScreen(name,title,url,direction) {
        this._controller.dispatchEvent(SaphoLauncher.EventType.CHANGE_SCREEN, {screenName: name,title: title,url: url,transitionDirection:direction});
    },

    /**
     * Destroy
     */
    destroy:function destroy() {
        SaphoLauncher.Command.prototype.destroy.call(this);

        if (this._googleApiRequest) {
            this._googleApiRequest.abort && this._googleApiRequest.abort();
            this._googleApiRequest = null;
        }

        if (this._saphoAuthRequest) {
            this._saphoAuthRequest.abort && this._saphoAuthRequest.abort();
            this._saphoAuthRequest = null;
        }

        if (this._saphoAccountRequest) {
            this._saphoAccountRequest.abort && this._saphoAccountRequest.abort();
            this._saphoAccountRequest = null;
        }

        if (this._inAppBrowser) {
            this._unRegisterBrowserEventListeners();
            this._inAppBrowser.close();
            this._inAppBrowser = null;
        }

        this._inAppBrowserListener = null;
        this._googleAuth = null;
        this._googleAccount = null;
    }
});
