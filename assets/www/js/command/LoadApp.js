/**
 * @class LoadApp
 * @extends {Command}
 * @param {{dispatchEvent:Function,stopExecution:Function}} controller
 * @constructor
 */
SaphoLauncher.LoadApp = function LoadApp(controller) {
    SaphoLauncher.Command.call(this,controller);

    this._originalUrl = null;
    this._storage = SaphoLauncher.ModelLocator.getProxy(SaphoLauncher.ModelName.STORAGE);
    this._iframe = null;
    this._running = false;
    this._delayTimeout = -1;
};

SaphoLauncher.LoadApp.prototype = Object.assign(Object.create(SaphoLauncher.Command.prototype), {
    /**
     * Execute the command
     *
     * @method execute
     * @param {{url:string,delay:number}} data
     */
    execute: function execute(data) {
        this._running = true;

        this._originalUrl = data.url;
        this._storage.setItem(SaphoLauncher.StorageKey.SAPHO_APP, this._originalUrl);

        var self = this;
        var url = this._originalUrl;
        var appUrl = url.toLowerCase().indexOf('http') === -1 ? 'http://' + url : url;

        if (!this._isStaticApp(appUrl)) appUrl = SaphoLauncher.UrlUtils.appendParams(appUrl, ["cjs", cordova.file.applicationDirectory]);

        this._delayTimeout = setTimeout(function() {
            // var fileTransfer = new FileTransfer();
            var uri = encodeURI(appUrl);
            var fileURL = cordova.file.cacheDirectory + 'app.html';

            self._removeCachedFile(fileURL, function() {
                new FileTransfer().download(
                    uri,
                    fileURL,
                    function(entry) {
                        $(document).remove('iframe');
                        var entryUrl = entry.toURL();

                        self._iframe = $('<iframe style="position:absolute;top:0;left:0;height:0px;width:0px;display:none;" height="0" width="0" src="' + uri + '"></iframe>');
                        self._iframe.load(function() {
                            if (self._running) {
                                var childWindow = self._iframe.get(0).contentWindow;

                                self._storage.setItem(SaphoLauncher.StorageKey.SAPHO_APP, childWindow.location.href);
                                if (!childWindow.r && !self._isStaticApp(childWindow.location.href)) {
                                    self._checkDefaultApp(appUrl);
                                } else {
                                    if (self._storage.getGoogleAccessToken() && entryUrl.indexOf("gtoken") === -1) window.location = SaphoLauncher.UrlUtils.appendParams(entryUrl, ["gtoken", self._storage.getGoogleAccessToken()]);
                                    else window.location = entryUrl;

                                    self._storage.saveRecentUrl(SaphoLauncher.UrlUtils.removeParams((self._originalUrl ? self._originalUrl : appUrl), ["cjs", "gtoken"]));

                                    self.dispatchEvent(SaphoLauncher.EventType.COMPLETE, self);
                                }
                            }
                        });
                        $('body').append(self._iframe);
                    },
                    function(error) {
                        navigator.notification.alert('', function() {
                            self._controller.dispatchEvent(SaphoLauncher.EventType.CHANGE_SCREEN, {
                                screenName: SaphoLauncher.ScreenName.LOGIN,
                                displayTypedUrl: true
                            });
                            self.dispatchEvent(SaphoLauncher.EventType.COMPLETE, self);
                        }, SaphoLauncher.ErrorMessage.UNABLE_TO_LOAD_APP, 'OK');
                    }
                );
            });
        },data.delay || 0);
    },

    /**
     * Attempt to remove cached files
     * @param {string} fileURL
     * @param {Function} callback
     * @private
     */
    _removeCachedFile: function _removeCachedFile(fileURL, callback) {
        var logFileError = function(error) {
            console.log("LogFileError: " + JSON.stringify(error));
            callback();
        };

        // Remove old file
        window.requestFileSystem(LocalFileSystem.TEMPORARY, 0, function(fileSystem) { // this returns the tmp folder
            // File found
            fileSystem.root.getFile(fileURL, {create: false, exclusive: true}, function(fileEntry) {
                fileEntry.remove(function(success) {
                    console.log(success);
                    callback();
                }, logFileError);
            }, logFileError);
        }, logFileError);
    },

    /**
     * Check for default app on server URL passed in
     * If the URL is not valid a Sapho app URL, check for default app on the server and load it if successfull
     * @method  checkDefaultApp
     * @param   {string} appUrl
     */
    _checkDefaultApp: function _checkDefaultApp(appUrl) {
        var components = appUrl.split("?"),
            url = components[0],
            params = components.length ? components[1] : "",
            apiUrl = null,
            self = this;

        url += url[url.length - 1] === "/" ? "" : "/";
        apiUrl = url + "api/v1/app/default/";

        $.ajax({
            url: apiUrl,
            contentType: 'application/json',
            dataType: 'json',
            success: function(resp) {
                if (params.indexOf("appid") > -1) params = params.replace(/appid=[0-9]*/ig, "appid=" + resp.applookupid);
                else params = "appid=" + resp.applookupid + "&" + params;
                url = SaphoLauncher.UrlUtils.removeParams(url + "renderer?" + params, ["cjs", "gtoken"]);
                this.execute(url);
            },
            error: function(error) {
                navigator.notification.alert('', function() {
                    self._controller.dispatchEvent(SaphoLauncher.EventType.CHANGE_SCREEN, {screenName: SaphoLauncher.ScreenName.LOGIN});
                    self.dispatchEvent(SaphoLauncher.EventType.COMPLETE, self);
                }, SaphoLauncher.ErrorMessage.INVALID_SAPHO_APP, 'OK');
            }
        });
    },

    /**
     * Check if URL passed in is listed among static apps
     * @param {string} url
     * @returns {boolean}
     * @private
     */
    _isStaticApp: function _isStaticApp(url) {
        return this._storage.getStaticUrls().filter(function(staticUrl) {return url.indexOf(staticUrl) > -1;}).length > 0;
    },

    /**
     * Destroy command
     * @private
     */
    destroy: function destroy() {
        this._running = false;

        clearTimeout(this._delayTimeout);

        SaphoLauncher.Command.prototype.destroy.call(this);

        if (this._iframe) {
            if (navigator.appName == 'Microsoft Internet Explorer') {
                this._iframe.document.execCommand('Stop');
            } else {
                this._iframe.stop();
            }
            $(document).remove('iframe');
            this._iframe = null;
        }
    }
});
