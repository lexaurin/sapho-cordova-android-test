/**
 * The Command
 * @class Command
 * @extends SaphoLauncher.EventDispatcher
 * @param {{dispatchEvent:Function,stopExecution:Function,init:Function}} controller
 */
SaphoLauncher.Command = function Command(controller) {
    SaphoLauncher.EventDispatcher.call(this);

    this._controller = controller;
};

SaphoLauncher.Command.prototype = Object.create(SaphoLauncher.EventDispatcher.prototype);

/**
 * Execute a command
 * @param {*=} data Defaults to null
 */
SaphoLauncher.Command.prototype.execute = function execute(data) {};

/**
 * Destroy current instance
 *
 * @method destroy
 */
SaphoLauncher.Command.prototype.destroy = function destroy() {
    SaphoLauncher.EventDispatcher.prototype.destroy.call(this);
};
