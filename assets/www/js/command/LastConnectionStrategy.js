/**
 * @class LastConnectionStrategy
 * @extends {Command}
 * @param {{dispatchEvent:Function,stopExecution:Function}} controller
 * @constructor
 */
SaphoLauncher.LastConnectionStrategy = function LastConnectionStrategy(controller) {
    SaphoLauncher.Command.call(this,controller);

    this._delay = 2000;
};

SaphoLauncher.LastConnectionStrategy.prototype = Object.assign(Object.create(SaphoLauncher.Command.prototype), {
    /**
     * Execute the command
     *
     * @method execute
     * @param {SaphoLauncher.Storage} storage
     */
    execute:function execute(storage) {
        try {
            var lastConnection = JSON.parse(storage.getItem(SaphoLauncher.StorageKey.LAST_CONNECTION)),
                title = lastConnection.title,
                connectingScreen = SaphoLauncher.ConnectingScreen;

            if (title === connectingScreen.DEMO_TITLE) {
                this._connectToApp(storage.getDemoUrl()," ",title)
            } else if (title === connectingScreen.CORPORATE_TITLE) {
                this._connectToApp(lastConnection.url,lastConnection.url,title);
            } else if (title === connectingScreen.GOOGLE_TITLE) {
                this._connectToGoogle();
            } else {
                this._changeScreen(SaphoLauncher.ScreenName.BUTTON);
            }
        } catch (e) {
            this._changeScreen(SaphoLauncher.ScreenName.BUTTON);
        }

        this.dispatchEvent(SaphoLauncher.EventType.COMPLETE, this);
    },

    /**
     * Connect to app at URL passed in
     * @param {string} url
     * @param {string} screenUrl
     * @param {string} title
     * @private
     */
    _connectToApp:function _connectToApp(url,screenUrl,title) {
        if (url && url.length) {
            this._changeScreen(SaphoLauncher.ScreenName.CONNECTING,title,screenUrl);
            this._controller.dispatchEvent(SaphoLauncher.EventType.LOAD_APP, {url: url,delay: this._delay});
        } else {
            this._changeScreen(SaphoLauncher.ScreenName.LOGIN);
        }
    },

    /**
     * Execute google login command
     * @private
     */
    _connectToGoogle:function _connectToGoogle() {
        this._controller.dispatchEvent(SaphoLauncher.EventType.GOOGLE_LOGIN, {delay: this._delay});
    },

    /**
     * Change screen to the one passed in
     * @param {string} screenName
     * @param {string} [title=null]
     * @param {string} [url=null]
     * @private
     */
    _changeScreen:function _changeScreen(screenName,title,url) {
        this._controller.dispatchEvent(SaphoLauncher.EventType.CHANGE_SCREEN, {screenName: screenName,title:title,url:url});
    }
});

