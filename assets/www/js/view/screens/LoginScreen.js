/**
 * LoginScreen
 *
 * @class LoginScreen
 * @extends SaphoLauncher.Screen
 * @param {HTMLElement} element
 * @param {{width:number,height:number}} layout
 * @param {{dispatchEvent:Function}} controller
 * @constructor
 */
SaphoLauncher.LoginScreen = function LoginScreen(element,layout,controller) {
    SaphoLauncher.Screen.call(this,element,layout,controller);

    this._LOGIN_BUTTON = "#corporate-login";
    this._BACK_BUTTON = ".footer-button";

    this._loginButton = this._element.find(this._LOGIN_BUTTON);
    this._backButton = this._element.find(this._BACK_BUTTON);
    this._serverUrlInput = this._element.find("#server-url");
    this._recentUrlsElement = this._element.find("#recent-urls");
    this._recentUrlsText = this._recentUrlsElement.find("span");
    this._recentUrlsSelect = this._recentUrlsElement.find("select");

    this._eventListeners[this._LOGIN_BUTTON] = this._onLoginButtonClick.bind(this);
    this._eventListeners[this._BACK_BUTTON] = this._onBackButtonClick.bind(this);
    this._eventListeners[SaphoLauncher.EventType.CHANGE] = this._onRecentUrlListChange.bind(this);
};

SaphoLauncher.LoginScreen.prototype = Object.assign(Object.create(SaphoLauncher.Screen.prototype), {
    /**
     * Update
     *
     * @param {{displayTypedUrl:boolean}} data
     */
    update:function update(data) {
        if (!data.displayTypedUrl) {
            var storage = SaphoLauncher.ModelLocator.getProxy(SaphoLauncher.ModelName.STORAGE),
                recentUrls = storage.getRecentUrls();

            this._serverUrlInput.val(storage.getAppUrl());

            this._hideRecentUrlList();
            if (recentUrls) this._showRecentUrlList(recentUrls.reverse());
        }

        this._serverUrlInput.removeAttr("disabled");
    },

    /**
     * Disable
     */
    disable:function disable() {
        SaphoLauncher.Screen.prototype.disable.call(this);

        this._serverUrlInput.attr("disabled", "disabled");
    },

    /**
     * Register event listeners
     * @private
     */
    _registerButtonEventListeners:function _registerButtonEventListeners() {
        this._element.css("pointer-events", "auto");
        this._loginButton.on(SaphoLauncher.EventType.CLICK, this._eventListeners[this._LOGIN_BUTTON]);
        this._backButton.on(SaphoLauncher.EventType.CLICK, this._eventListeners[this._BACK_BUTTON]);
        document.addEventListener(SaphoLauncher.EventType.BACK_BUTTON, this._eventListeners[this._BACK_BUTTON], false);
    },

    /**
     * UnRegister event listeners
     * @private
     */
    _unRegisterButtonEventListeners:function _unRegisterButtonEventListeners() {
        this._loginButton.off(SaphoLauncher.EventType.CLICK, this._eventListeners[this._LOGIN_BUTTON]);
        this._backButton.off(SaphoLauncher.EventType.CLICK, this._eventListeners[this._BACK_BUTTON]);
        document.removeEventListener(SaphoLauncher.EventType.BACK_BUTTON, this._eventListeners[this._BACK_BUTTON], false);
        this._element.css("pointer-events", "none");
    },

    /**
     * Login button click handler
     * @param {Event} e
     * @private
     */
    _onLoginButtonClick:function _onLoginButtonClick(e) {
        var url = this._serverUrlInput.val();
        if (url && url.length) {
            this._controller.dispatchEvent(SaphoLauncher.EventType.CHANGE_SCREEN, {
                screenName: SaphoLauncher.ScreenName.CONNECTING,
                title: SaphoLauncher.ConnectingScreen.CORPORATE_TITLE,
                url: url,
                transitionDirection:SaphoLauncher.Direction.LEFT
            });
            this._controller.dispatchEvent(SaphoLauncher.EventType.LOAD_APP, {
                url: url,
                delay:2000
            });
        }
    },

    /**
     * Back button click handler
     * @param {Event} e
     * @private
     */
    _onBackButtonClick:function _onBackButtonClick(e) {
        this._controller.dispatchEvent(SaphoLauncher.EventType.CHANGE_SCREEN, {screenName: SaphoLauncher.ScreenName.BUTTON});
    },

    /**
     * Generate and display list of urls passed in
     * @method _showRecentUrlList
     * @param  {Array.<string>} urls
     */
    _showRecentUrlList:function _showRecentUrlList(urls) {
        this._recentUrlsElement.css("display", "table");

        for (var i = 0, l = urls.length; i < l; i++) this._recentUrlsSelect.append($("<option />", {
            value: urls[i],
            text: urls[i]
        }));

        this._recentUrlsSelect.bind("change", this._eventListeners[SaphoLauncher.EventType.CHANGE]);
    },

    /**
     * Hide and clears list of recent urls
     * @method  _hideRecentUrlList
     */
    _hideRecentUrlList:function _hideRecentUrlList() {
        this._recentUrlsElement.css("display", "none");

        this._recentUrlsSelect.unbind("change", this._eventListeners[SaphoLauncher.EventType.CHANGE]);

        this._recentUrlsSelect.find(">option").each(function(index, option) {
            if ($(this).attr("disabled") !== "disabled") $(this).remove();
        });
    },

    /**
     * On recent-urls list change event
     * @method  _onRecentUrlListChange
     * @param   {Event} e
     */
    _onRecentUrlListChange:function _onRecentUrlListChange(e) {
        var selectedUrl = this._recentUrlsSelect.val();
        if (selectedUrl && selectedUrl.length) {
            this._serverUrlInput.val(selectedUrl);
            this._recentUrlsText.html(selectedUrl);
        }
    }
});
