/**
 * ConnectingScreen
 *
 * @class ConnectingScreen
 * @extends SaphoLauncher.Screen
 * @param {HTMLElement} element
 * @param {{width:number,height:number}} layout
 * @param {{dispatchEvent:Function,stopExecution:Function}} controller
 * @constructor
 */
SaphoLauncher.ConnectingScreen = function ConnectingScreen(element,layout,controller) {
    SaphoLauncher.Screen.call(this,element,layout,controller);

    this._titleElement = this._element.find(".screen-title strong");
    this._serverUrlElement = this._element.find(".connecting-url strong");
    this._backButton = this._element.find(".footer-button");
    this._backButtonIcon = this._backButton.find("i");
    this._backButtonText = this._backButton.find("span");

    this._eventListeners[SaphoLauncher.EventType.CLICK] = this._onBackButtonClick.bind(this);
};

SaphoLauncher.ConnectingScreen.prototype = Object.create(SaphoLauncher.Screen.prototype);

SaphoLauncher.ConnectingScreen.DEMO_TITLE = "Connecting to Demo Server";
SaphoLauncher.ConnectingScreen.CORPORATE_TITLE = "Connecting to Corporate Server";
SaphoLauncher.ConnectingScreen.GOOGLE_TITLE = "Connecting to Google for Work Server";

/**
 * Update screen with data passed in
 * @method  update
 * @param   {{screenName:string,title:string,url:string}} data
 */
SaphoLauncher.ConnectingScreen.prototype.update = function update(data) {
    if (data.title) this._titleElement.html(data.title);
    if (data.url) this._serverUrlElement.html(SaphoLauncher.UrlUtils.getHostName(data.url));

    this._updateBackButton(data.title);

    try {
        SaphoLauncher.ModelLocator.getProxy(SaphoLauncher.ModelName.STORAGE).setItem(SaphoLauncher.StorageKey.LAST_CONNECTION,JSON.stringify(data));
    } catch (e) {}
};

/**
 * Update back button icon and text according to title passed in
 * @param {string} title
 * @private
 */
SaphoLauncher.ConnectingScreen.prototype._updateBackButton = function _updateBackButton(title) {
    if (title === SaphoLauncher.ConnectingScreen.DEMO_TITLE) {
        this._backButtonIcon.attr("class","fa-arrow-circle-left");
        this._backButtonText.html("  Go Back");
    } else if (title === SaphoLauncher.ConnectingScreen.CORPORATE_TITLE) {
        this._backButtonIcon.attr("class","fa-sign-out");
        this._backButtonText.html("  Change Server");
    } else if (title === SaphoLauncher.ConnectingScreen.GOOGLE_TITLE) {
        this._backButtonIcon.attr("class","fa-sign-out");
        this._backButtonText.html("  Switch Accounts");
    }
};

/**
 * Register event listeners
 * @private
 */
SaphoLauncher.ConnectingScreen.prototype._registerButtonEventListeners = function _registerButtonEventListeners() {
    this._backButton.on(SaphoLauncher.EventType.CLICK,this._eventListeners[SaphoLauncher.EventType.CLICK]);
    document.addEventListener(SaphoLauncher.EventType.BACK_BUTTON,this._eventListeners[SaphoLauncher.EventType.CLICK],false);
};

/**
 * UnRegister event listeners
 * @private
 */
SaphoLauncher.ConnectingScreen.prototype._unRegisterButtonEventListeners = function _unRegisterButtonEventListeners() {
    this._backButton.off(SaphoLauncher.EventType.CLICK,this._eventListeners[SaphoLauncher.EventType.CLICK]);
    document.removeEventListener(SaphoLauncher.EventType.BACK_BUTTON,this._eventListeners[SaphoLauncher.EventType.CLICK],false);
};

/**
 * Back button click handler
 * @method  _onBackButtonClick
 * @param   {Event} e
 */
SaphoLauncher.ConnectingScreen.prototype._onBackButtonClick = function _onBackButtonClick(e) {
    this._controller.stopExecution();

    var title = this._titleElement.html();
    if (title === SaphoLauncher.ConnectingScreen.CORPORATE_TITLE) {
        this._controller.dispatchEvent(SaphoLauncher.EventType.CHANGE_SCREEN,{screenName:SaphoLauncher.ScreenName.LOGIN});
    } else if (title === SaphoLauncher.ConnectingScreen.DEMO_TITLE) {
        this._controller.dispatchEvent(SaphoLauncher.EventType.CHANGE_SCREEN,{screenName:SaphoLauncher.ScreenName.BUTTON});
    } else if (title === SaphoLauncher.ConnectingScreen.GOOGLE_TITLE) {
        this._controller.dispatchEvent(SaphoLauncher.EventType.GOOGLE_LOGIN,{switch:true},true);
    }
};
