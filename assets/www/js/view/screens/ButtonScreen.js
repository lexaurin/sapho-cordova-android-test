/**
 * ButtonScreen
 *
 * @class ButtonScreen
 * @extends SaphoLauncher.Screen
 * @param {HTMLElement} element
 * @param {{width:number,height:number}} layout
 * @param {{dispatchEvent:Function}} controller
 * @constructor
 */
SaphoLauncher.ButtonScreen = function ButtonScreen(element,layout,controller) {
    SaphoLauncher.Screen.call(this,element,layout,controller);

    this._DEMO_BUTTON = "#check-out-demo";
    this._CORPORATE_BUTTON = "#corporate-server";
    this._GOOGLE_BUTTON = "#google-login";
    
    this._demoButton = this._element.find(this._DEMO_BUTTON);
    this._corporateServerButton = this._element.find(this._CORPORATE_BUTTON);
    this._googleLoginButton = this._element.find(this._GOOGLE_BUTTON);
};

SaphoLauncher.ButtonScreen.prototype = Object.assign(Object.create(SaphoLauncher.Screen.prototype),{
    /**
     * Bind event listeners to 'this' scope
     * @private
     */
    _bindEventListeners:function _bindEventListeners() {
        this._eventListeners[this._DEMO_BUTTON] = this._eventListeners[this._DEMO_BUTTON] || this._onDemoButtonClick.bind(this);
        this._eventListeners[this._CORPORATE_BUTTON] = this._eventListeners[this._CORPORATE_BUTTON] || this._onCorporateServerButtonClick.bind(this);
        this._eventListeners[this._GOOGLE_BUTTON] = this._eventListeners[this._GOOGLE_BUTTON] || this._onGoogleLoginButtonClick.bind(this);
    },

    /**
     * Register event listeners
     * @private
     */
    _registerButtonEventListeners:function _registerButtonEventListeners() {
        if (!this._eventsRegistered) {
            this._eventsRegistered = true;

            this._bindEventListeners();

            var clickEventType = SaphoLauncher.EventType.CLICK;
            this._demoButton.on(clickEventType, this._eventListeners[this._DEMO_BUTTON]);
            this._corporateServerButton.on(clickEventType, this._eventListeners[this._CORPORATE_BUTTON]);
            this._googleLoginButton.on(clickEventType, this._eventListeners[this._GOOGLE_BUTTON]);
        }
    },

    /**
     * UnRegister event listeners
     * @private
     */
    _unRegisterButtonEventListeners:function _unRegisterButtonEventListeners() {
        var clickEventType = SaphoLauncher.EventType.CLICK;
        this._demoButton.off(clickEventType, this._eventListeners[this._DEMO_BUTTON]);
        this._corporateServerButton.off(clickEventType, this._eventListeners[this._CORPORATE_BUTTON]);
        this._googleLoginButton.off(clickEventType, this._eventListeners[this._GOOGLE_BUTTON]);

        this._eventsRegistered = false;
    },

    /**
     * Demo button click handler
     * @method  _onDemoButtonClick
     * @param   {Event} e
     */
    _onDemoButtonClick:function _onDemoButtonClick(e) {
        this._controller.dispatchEvent(SaphoLauncher.EventType.CHANGE_SCREEN, {
            screenName: SaphoLauncher.ScreenName.CONNECTING,
            title: SaphoLauncher.ConnectingScreen.DEMO_TITLE,
            url: " ",
            transitionDirection:SaphoLauncher.Direction.LEFT
        });
        this._controller.dispatchEvent(SaphoLauncher.EventType.LOAD_APP, {
            url: SaphoLauncher.ModelLocator.getProxy(SaphoLauncher.ModelName.STORAGE).getDemoUrl(),
            delay:2000
        });
    },

    /**
     * Corporate button click event
     * @method  _onCorporateServerButtonClick
     * @param   {Event} e
     */
    _onCorporateServerButtonClick:function _onCorporateServerButtonClick(e) {
        this._controller.dispatchEvent(SaphoLauncher.EventType.CHANGE_SCREEN, {
            screenName: SaphoLauncher.ScreenName.LOGIN,
            transitionDirection:SaphoLauncher.Direction.LEFT
        });
    },

    /**
     * Google login button click handler
     * @method  _onGoogleLoginButtonClick
     * @param   {Event} e
     */
    _onGoogleLoginButtonClick:function _onGoogleLoginButtonClick(e) {
        this._controller.dispatchEvent(SaphoLauncher.EventType.GOOGLE_LOGIN);
    }
});
