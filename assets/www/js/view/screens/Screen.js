/**
 * Abstract Screen
 *
 * @class Screen
 * @extends SaphoLauncher.EventDispatcher
 * @param {Object} element
 * @param {Object} layout
 * @param {{dispatchEvent:Function,stopExecution:Function}} controller
 * @constructor
 */
SaphoLauncher.Screen = function Screen(element,layout,controller) {
    SaphoLauncher.EventDispatcher.call(this);

    this._element = element;
    this._layout = layout;
    this._controller = controller;
    this._layoutDirty = true;
    this._x = 0.0;
    this._y = 0.0;
    this._enabled = false;
    this._eventsRegistered = false;
    this._eventListeners = {};

    this._transitionState = SaphoLauncher.TransitionState.HIDDEN;
    this._transitionDirection = SaphoLauncher.Direction.RIGHT;
    this._ticker = SaphoLauncher.ModelLocator.getProxy(SaphoLauncher.ModelName.TICKER);
    this._showHideTween = new SaphoLauncher.TweenProxy(1,SaphoLauncher.Easing.inOutExpo);
};

SaphoLauncher.Screen.prototype = Object.assign(Object.create(SaphoLauncher.EventDispatcher.prototype), {
    /**
     * Update with data passed in
     * @param {Object} data
     */
    update:function update(data) {},

    /**
     * Show
     * @param {string} transitionDirection
     */
    show:function show(transitionDirection) {
        this._transitionDirection = transitionDirection;

        var TransitionState = SaphoLauncher.TransitionState;
        if (this._transitionState === TransitionState.HIDDEN || this._transitionState === TransitionState.HIDING) {
            this._element.css({opacity: 0, display: "block", margin: 0, position: "absolute"});

            if (this._layoutDirty) this._updateLayout();

            this.enable();

            this._transitionState = TransitionState.SHOWING;
            this._updateTween(0);
            this._showHideTween.start(true);
        }
    },

    /**
     * Hide
     * @param {string} transitionDirection
     */
    hide:function hide(transitionDirection) {
        this._transitionDirection = transitionDirection;

        var TransitionState = SaphoLauncher.TransitionState;
        if (this._transitionState === TransitionState.SHOWN || this._transitionState === TransitionState.SHOWING) {
            this._transitionState = TransitionState.HIDING;

            this._unRegisterButtonEventListeners();
            this._updateTween(0);

            this._showHideTween.start(true);
        }
    },

    /**
     * Enable
     */
    enable:function enable() {
        if (!this._enabled) {
            this._registerEventListeners();
            this._enabled = true;
        }
    },

    /**
     * Disable
     */
    disable:function disable() {
        this._unRegisterEventListeners();
        this._enabled = false;
    },

    /**
     * Register event listeners
     * @protected
     */
    _registerEventListeners:function _registerEventListeners() {
        this._ticker.addEventListener(SaphoLauncher.EventType.TICK, this, this._onTick);
        this._showHideTween.addEventListener(SaphoLauncher.EventType.COMPLETE, this, this._onTweenComplete);
    },

    /**
     * UnRegister event listeners
     * @protected
     */
    _unRegisterEventListeners:function _unRegisterEventListeners() {
        this._ticker.removeEventListener(SaphoLauncher.EventType.TICK, this, this._onTick);
        this._showHideTween.removeEventListener(SaphoLauncher.EventType.COMPLETE, this, this._onTweenComplete);
    },

    /**
     * Register button event listeners
     * @protected
     */
    _registerButtonEventListeners:function _registerButtonEventListeners() {
        // abstract
    },

    /**
     * UnRegister button event listeners
     * @protected
     */
    _unRegisterButtonEventListeners:function _unRegisterButtonEventListeners() {
        // abstract
    },

    /**
     * On tick
     * @private
     */
    _onTick:function _onTick() {
        if (this._showHideTween.isRunning()) this._updateTween(this._showHideTween.progress);
    },

    /**
     * Update tween
     * @param {number} progress
     * @private
     */
    _updateTween:function _updateTween(progress) {
        var TransitionState = SaphoLauncher.TransitionState,
            w = this._layout.width,
            ew = this._element.width(),
            center = Math.round((w - ew) / 2);

        if (this._transitionState === TransitionState.SHOWING) {
            this._element.css({opacity: progress});

            if (this._transitionDirection === SaphoLauncher.Direction.RIGHT) this._x = -ew + (center + ew) * progress;
            else this._x = center + (center + ew) * (1 - progress);
        } else if (this._transitionState === TransitionState.HIDING) {
            this._element.css({opacity: 1 - progress});

            if (this._transitionDirection === SaphoLauncher.Direction.RIGHT) this._x = center + (center + ew) * progress;
            else this._x = -ew + (center + ew) * (1 - progress);
        }

        this._updatePosition();
    },

    /**
     * On tween complete
     * @private
     */
    _onTweenComplete:function _onTweenComplete() {
        var TransitionState = SaphoLauncher.TransitionState;

        this._updateTween(1);

        if (this._transitionState === TransitionState.SHOWING) {
            this._transitionState = TransitionState.SHOWN;
            this._registerButtonEventListeners();
        } else if (this._transitionState === TransitionState.HIDING) {
            this._transitionState = TransitionState.HIDDEN;
            this.disable();
            this._element.css("display", "none");
            this._x = 0;
            this._updatePosition();

            this.dispatchEvent(SaphoLauncher.EventType.COMPLETE, {target: this, state: this._transitionState});
        }
    },

    /**
     * Resize
     */
    resize:function resize() {
        this._layoutDirty = true;

        if (this._transitionState === SaphoLauncher.TransitionState.SHOWING || this._transitionState === SaphoLauncher.TransitionState.SHOWN) {
            this._updateLayout();
        }
    },

    /**
     * Update screen layout
     * @method  _updateLayout
     */
    _updateLayout:function() {
        this._layoutDirty = false;

        this._x = Math.round((this._layout.width - this._element.width()) / 2);
        this._y = Math.round((this._layout.height - this._element.height()) / 2);
        if (this._y < 10) this._y = 10;

        this._updatePosition();
    },

    /**
     * Update position
     * @private
     */
    _updatePosition:function _updatePosition() {
        this._element.css("transform", "translate3d(" + this._x + "px," + this._y + "px,0)");
    }
});
