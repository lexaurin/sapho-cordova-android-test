/**
 * @class ApplicationView
 * @param {{width:number,height:number}} layout
 * @param {Object} screens
 * @constructor
 */
SaphoLauncher.ApplicationView = function ApplicationView(layout,screens) {
    this._layout = layout;
    this._bodyElement = $('body');
    this._screens = screens;
    this._currentScreen = null;
    
    this._init();
};

SaphoLauncher.ApplicationView.prototype = {
    /**
     * Initialize
     * @private
     */
    _init:function _init() {
        this._registerEventListeners();
    },

    /**
     * Register event listeners
     * @method  _registerEventListeners
     */
    _registerEventListeners:function _registerEventListeners() {
        window.addEventListener(SaphoLauncher.EventType.RESIZE, this._onResize.bind(this), false);
    },

    /**
     * Resize handler
     * @method  _onResize
     * @param   {Event} e
     */
    _onResize:function _onResize(e) {
        this._layout.width = window.innerWidth;
        this._layout.height = window.innerHeight;

        this._bodyElement.scrollTop(0);
        this._bodyElement.scrollLeft(0);

        for (var screen in this._screens) this._screens[screen].resize();
    },

    /**
     * On screen change
     * @param {{screenName:string,transitionDirection:string}} data
     * @private
     */
    changeScreen:function changeScreen(data) {
        if (data.screenName) {
            if (this._currentScreen === this._screens[data.screenName]) {
                this._currentScreen.update(data);
            } else {
                if (this._currentScreen) this._currentScreen.hide(data.transitionDirection || SaphoLauncher.Direction.RIGHT);
                this._currentScreen = this._screens[data.screenName];
                this._currentScreen.update(data);
                this._currentScreen.show(data.transitionDirection || SaphoLauncher.Direction.RIGHT);
            }
        }
    }
};
