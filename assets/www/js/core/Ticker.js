/**
 * @class Ticker
 * @constructor
 */
SaphoLauncher.Ticker = function Ticker()
{
    SaphoLauncher.EventDispatcher.call(this);

    this._rafListener = this._raf.bind(this);
    this._isRunning = false;
};

SaphoLauncher.Ticker.prototype = Object.create(SaphoLauncher.EventDispatcher.prototype);

/**
 * Add event listener
 * @param {string} eventType
 * @param {Object} scope
 * @param {Function} listener
 */
SaphoLauncher.Ticker.prototype.addEventListener = function(eventType,scope,listener) {
    SaphoLauncher.EventDispatcher.prototype.addEventListener.call(this,eventType,scope,listener);

    if (!this._isRunning) {
        this._isRunning = true;

        window.requestAnimationFrame(this._rafListener);
    }
};

/**
 * Remove event listeners
 * @param {string} eventType
 * @param {Object} scope
 * @param {Function} listener
 */
SaphoLauncher.Ticker.prototype.removeEventListener = function(eventType,scope,listener) {
    SaphoLauncher.EventDispatcher.prototype.removeEventListener.call(this,eventType,scope,listener);

    if (this._listeners.length === 0) this._isRunning = false;
};

/**
 * Remove all listeners
 */
SaphoLauncher.Ticker.prototype.removeAllListeners = function() {
    SaphoLauncher.EventDispatcher.prototype.removeAllListeners.call(this);

    this._isRunning = false;
};

/**
 * Animation function
 * @private
 */
SaphoLauncher.Ticker.prototype._raf = function _raf() {
    if (this._isRunning) {
        window.requestAnimationFrame(this._rafListener);

        this.dispatchEvent(SaphoLauncher.EventType.TICK);
    }
};
