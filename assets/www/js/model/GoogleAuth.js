/**
 * @class GoogleAuth
 * @param {SaphoLauncher.Storage} storage
 * @constructor
 */
SaphoLauncher.GoogleAuth = function GoogleAuth(storage) {
    this._authUrlTemplate = "{0}/o/oauth2/auth?scope={1}&redirect_uri={2}&state=uri{3}&response_type=token&client_id={4}&access_type=online&include_granted_scopes=true";
    this._host = "https://accounts.google.com";
    this._clientId = '59714311932-jo597ve83psv69iflef8tvuai80pordq.apps.googleusercontent.com';
    this._authCallback = "http://auth.sapho.com/callback";

    this._storage = storage;
};

SaphoLauncher.GoogleAuth.prototype = {
    /**
     * Check whether the url passed in is successful redirection from google auth
     * @param {string} url
     * @returns {boolean}
     */
    authenticated:function authenticated(url) {
        return url.indexOf(this._authCallback) > -1 && url.indexOf("access_token") > -1;
    },
    /**
     * Checks if the url passed in navigates to google-auth login form
     * @param {string} url
     * @returns {boolean}
     */
    formUrl:function formUrl(url) {
        return url.indexOf(this._host) === 0 && SaphoLauncher.UrlUtils.getPathName(url).indexOf("oauth2") === -1
    }
};

/**
 * @property host
 * @type string
 */
Object.defineProperty(SaphoLauncher.GoogleAuth.prototype,'host',{
    get:function() {
        return this._host;
    }
});

/**
 * @property authUrl
 * @type string
 */
Object.defineProperty(SaphoLauncher.GoogleAuth.prototype,'authUrl',{
    get:function() {
        return new Template(this._authUrlTemplate,this._host,"email profile",this._authCallback,this._storage.getAppUrl(),this._clientId).toString();
    }
});
