/**
 * @class GoogleAccount
 * @param {SaphoLauncher.Storage} storage
 * @constructor
 */
SaphoLauncher.GoogleAccount = function GoogleAccount(storage) {
    this._accessToken = null;
    this._email = null;

    this._storage = storage;
};

SaphoLauncher.GoogleAccount.prototype = {
    /**
     * Reset accessToken and email values
     */
    reset:function reset() {
        this._accessToken = null;
        this._email = null;
        this._storage.resetGoogleAccessToken();
    }
};

/**
 * @property accessToken
 * @type string
 */
Object.defineProperty(SaphoLauncher.GoogleAccount.prototype,'accessToken',{
    get:function() {
        return this._accessToken;
    },
    set:function(value) {
        this._accessToken = value;
        this._storage.saveGoogleAccessToken(value);
    }
});

/**
 * @property email
 * @type string
 */
Object.defineProperty(SaphoLauncher.GoogleAccount.prototype,'email',{
    get:function() {
        return this._email;
    },
    set:function(value) {
        this._email = value;
    }
});