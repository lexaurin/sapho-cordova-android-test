/**
 * Storage
 * Helper methods around localStorage
 * @constructor
 */
SaphoLauncher.Storage = function Storage() {
    this._demoUrl = "http://demoapp.sapho.com/polymer";
    this._staticUrls = ['streamdev.arrakis.sapho.com'];
    this._googleAccessToken = null;
};

SaphoLauncher.Storage.prototype = {
    /**
     * Save URL passed in into list of recent URLs
     * Limit the list to 5 URLs
     * @method  saveRecentUrl
     * @param   {string} url
     */
    saveRecentUrl:function saveRecentUrl(url) {
        if (url && url.length && url !== this._demoUrl) {
            var storedUrls = window.localStorage.getItem(SaphoLauncher.StorageKey.RECENT_URLS);
            if (storedUrls && storedUrls.length) {
                var urls = storedUrls.split(",");
                // Keep the list of urls to maximum 5
                if (urls.length >= 5) urls.splice(0, urls.length - 5);
                // If url is already saved, remove it so the url is added at the end
                if (urls.indexOf(url) > -1) urls.splice(urls.indexOf(url), 1);

                urls.push(url);
                window.localStorage.setItem(SaphoLauncher.StorageKey.RECENT_URLS, urls);
            } else {
                window.localStorage.setItem(SaphoLauncher.StorageKey.RECENT_URLS, url);
            }
        }
    },

    /**
     * Check if any recent urls are saved and if so convert them into array and return
     * @method  _getRecentUrls
     * @return {Array|null} array of urls
     */
    getRecentUrls:function getRecentUrls() {
        var storedUrls = window.localStorage.getItem(SaphoLauncher.StorageKey.RECENT_URLS);
        return storedUrls && storedUrls.length ? storedUrls.split(",") : null;
    },

    /**
     * Get the app url.
     *
     * @return string
     */
    getAppUrl:function getAppUrl() {
        var recentUrls = this.getRecentUrls(),
            appUrl = null;

        if (recentUrls && recentUrls.length) appUrl = recentUrls[recentUrls.length - 1];
        else appUrl = window.localStorage.getItem(SaphoLauncher.StorageKey.SAPHO_APP);

        if (!appUrl || appUrl === 'http://apps.sapho.com/apps/contacts') appUrl = this._demoUrl;

        return SaphoLauncher.UrlUtils.removeParams(appUrl, ["cjs", "gtoken"]);
    },

    /**
     * Return google access token
     * @returns {string}
     */
    getGoogleAccessToken:function getGoogleAccessToken() {
        return this._googleAccessToken;
    },

    /**
     * Save google access token
     * @param value
     */
    saveGoogleAccessToken:function saveGoogleAccessToken(value) {
        this._googleAccessToken = value;
    },

    /**
     * Reset google access token
     */
    resetGoogleAccessToken:function resetGoogleAccessToken() {
        this._googleAccessToken = null;
    },

    /**
     * Return demo url
     * @returns {string}
     */
    getDemoUrl:function getDemoUrl() {
        return this._demoUrl;
    },

    /**
     * Return array of static app URLs
     * @returns {Array.<string>}
     */
    getStaticUrls:function getStaticUrls() {
        return this._staticUrls;
    },

    /**
     * Save value under key passed in
     * @param {string} key
     * @param {string} value
     */
    setItem:function setItem(key, value) {
        if (key === SaphoLauncher.StorageKey.SAPHO_APP && value === this._demoUrl) return;
        window.localStorage.setItem(key, value);
    },

    /**
     * Retrieve and return value for the key passed in
     * @param {string} key
     */
    getItem:function getItem(key) {
        return window.localStorage.getItem(key);
    }
};
