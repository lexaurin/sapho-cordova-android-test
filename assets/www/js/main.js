(function() {
    /**
     * Initialize Sapho launcher
     * @method  init
     */
    function init() {
        SaphoLauncher.Controller.add(SaphoLauncher.EventType.INITIALIZE,SaphoLauncher.Initialize);
        SaphoLauncher.Controller.dispatchEvent(SaphoLauncher.EventType.INITIALIZE);
    }

     document.addEventListener('deviceready',init,false);
    //init();
})();
