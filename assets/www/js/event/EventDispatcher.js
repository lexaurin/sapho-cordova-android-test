/**
 * @class EventDispatcher
 * @constructor
 */
SaphoLauncher.EventDispatcher = function EventDispatcher() {
    this._listeners = [];
};

/**
 * Add event listener
 * @param	{string} eventType
 * @param	{Object} scope
 * @param	{Function} listener
 */
SaphoLauncher.EventDispatcher.prototype.addEventListener = function addEventListener(eventType,scope,listener) {
    if (!this.hasEventListener(eventType,scope,listener)) {
        this._listeners[this._listeners.length] = {
            type:eventType,
            scope:scope,
            handler:listener
        };
    }
};

/**
 * @method hasEventListener
 * @param	{string} eventType
 * @param	{Object} scope
 * @param	{Function} handler
 * @return  {boolean}
 */
SaphoLauncher.EventDispatcher.prototype.hasEventListener = function hasEventListener(eventType,scope,handler) {
    var i = 0, l = this._listeners.length, listener = null;
    for (;i<l;) {
        listener = this._listeners[i++];
        if (listener.type === eventType && listener.scope === scope && listener.handler === handler) {
            listener = null;
            return true;
        }
    }
    listener = null;

    return false;
};

/**
 * Remove event listener
 * @param	{String} eventType
 * @param	{Object} scope
 * @param	{Function} handler
 */
SaphoLauncher.EventDispatcher.prototype.removeEventListener = function removeEventListener(eventType,scope,handler) {
    var i = 0, l = this._listeners.length, listener = null;
    for (;i<l;i++) {
        listener = this._listeners[i];
        if (listener.type === eventType && listener.scope === scope && listener.handler === handler) {
            this._listeners.splice(i,1);
            listener.scope = null;
            listener.handler = null;

            break;
        }
    }
    listener = null;
};

/**
 * Remove all listeners
 */
SaphoLauncher.EventDispatcher.prototype.removeAllListeners = function removeAllListeners() {
    var i = 0, l = this._listeners.length, listener = null;
    for (;i<l;i++) {
        listener = this._listeners[i];
        
        this._listeners.splice(i,1);
        listener.scope = null;
        listener.handler = null;
    }
    listener = null;
    this._listeners.length = 0;
};

/**
 * Dispatch event
 * @param {string} eventType
 * @param {Object|null} data
 */
SaphoLauncher.EventDispatcher.prototype.dispatchEvent = function dispatchEvent(eventType,data) {
    var i = 0, l = this._listeners.length, listener = null;
    for (;i<l;) {
        listener = this._listeners[i++];
        if (listener && listener.type === eventType) {
            listener.handler.call(listener.scope,data,eventType);
        }
    }
    listener = null;
};

/**
 * @method pipe Link incoming and outcoming events; dispatch incoming events further
 * @param {Object} target
 * @param {string} eventType
 */
SaphoLauncher.EventDispatcher.prototype.pipe = function pipe(target,eventType) {
    target.addEventListener(eventType,this,this._pipeListener);
};

/**
 * @method unpipe Remove event target from pipe
 * @param {Object} target
 * @param {string} eventType
 */
SaphoLauncher.EventDispatcher.prototype.unPipe = function unPipe(target,eventType) {
    target.removeEventListener(eventType,this,this._pipeListener);
};

/**
 * @method pipeListener Listens for events piped in, and dispatch them further
 * @param {string} eventType
 * @param {Object|null} data
 * @private
 */
SaphoLauncher.EventDispatcher.prototype._pipeListener = function _pipeListener(data,eventType) {
    this.dispatchEvent(eventType,data);
};

/**
 * Destroy
 */
SaphoLauncher.EventDispatcher.prototype.destroy = function destroy() {
    this.removeAllListeners();

    this._listeners.length = 0;
};
