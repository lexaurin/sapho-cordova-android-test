/**
 * @class Controller
 * @type {{_eventCommandMap: {}, _commands: Array, _init: Function, _onCommandComplete: Function, _destroyCommand: Function, dispatchEvent: Function}}
 */
SaphoLauncher.Controller = {
    _eventCommandMap:{},
    /** @type {Array.<SaphoLauncher.Command>} */
    _commands:[],

    /**
     * Init
     * @param {Array.<>} eventMap
     */
    init:function init(eventMap) {
        for (var i=0,l=eventMap.length;i<l;) this._eventCommandMap[eventMap[i++]] = {constructor:eventMap[i++]};
    },

    /**
     * Add command with eventType
     * @param {string} eventType
     * @param {Function} command
     */
    add:function add(eventType,command) {
        this._eventCommandMap[eventType] = {constructor:command};
    },

    /**
     * On command complete
     * @param {*} data
     * @private
     */
    _onCommandComplete:function _onCommandComplete(data) {
        this._destroyCommand(data);
    },

    /**
     * Destroy command
     * @param {SaphoLauncher.Command} command
     * @private
     */
    _destroyCommand:function _destroyCommand(command) {
        for (var i=0,l=this._commands.length,cmd=null;i<l;i++) {
            cmd = this._commands[i];
            if (cmd === command) {
                cmd.removeEventListener(SaphoLauncher.EventType.COMPLETE,this,this._onCommandComplete);
                cmd.destroy();
                this._commands.splice(i,1);
                break;
            }
        }
    },

    /**
     * Dispatch event passed in
     * @param {string} eventType
     * @param {*} [data=null] Defaults to null
     * @param {boolean} [checkRunningInstances=false] Defaults to false
     */
    dispatchEvent:function dispatchEvent(eventType,data,checkRunningInstances) {
        /** @type {Function} */var commandConstructor = this._eventCommandMap[eventType].constructor;
        if (commandConstructor) {
            /** @type {SaphoLauncher.Command} */var cmd = null;

            // First check, if multiple instances of this Command are allowed
            // If not, destroy running instances, before executing new one
            if (checkRunningInstances) {
                for (var i=0,l=this._commands.length;i<l;i++) {
                    cmd = this._commands[i];
                    if (cmd instanceof commandConstructor) {
                        this._destroyCommand(cmd);
                    }
                }
            }

            // Execute command
            cmd = /** @type {SaphoLauncher.Command} */new commandConstructor(this);

            this._commands.push(cmd);

            cmd.addEventListener(SaphoLauncher.EventType.COMPLETE,this,this._onCommandComplete);
            cmd.execute(data);
        }
    },

    /**
     * Stop any running command
     */
    stopExecution:function stopExecution() {
        for (var i= 0,l=this._commands.length,cmd=null;i<l;i++) {
            cmd = this._commands[i];
            cmd.removeEventListener(SaphoLauncher.EventType.COMPLETE,this,this._onCommandComplete);
            cmd.destroy();
        }
        this._commands.length = 0;
    }
};
